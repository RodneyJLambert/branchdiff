//Copyright 2019 Rodney J. Lambert
//
//Permission is hereby granted, free of charge, to any person to use, copy, or modify this software in commercial
//and non-commercial settings. If redistributed please include this notice.
//
//THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED.

#include <iostream>
#include <array>
#include <memory>
#include <sstream>
#include <vector>
#include <algorithm>
#include <experimental/filesystem>
#include <stdio.h>

#ifdef WIN32
std::string DeleteFileCommand = "del";
std::string TempDirSlash = "";
#else
std::string DeleteFileCommand = "rm";
std::string TempDirSlash = "/";
#endif


enum class UserInput
{
	Quit,
	Yes,
	No
};

std::string
GetStringUpToNewLine(const std::string &multiLine, size_t &index)
{
	size_t nextNewLine = multiLine.find('\n', index);

	if(nextNewLine == std::string::npos)
	{
	    std::string line = multiLine.substr(index);
	    index = std::string::npos;
		return line;
	}
	std::string sub = multiLine.substr(index, nextNewLine - index);
	index = nextNewLine + 1;
	return sub;
}

std::string
GetStringUpToNewLine(const std::string &multiLine)
{
	size_t index = 0;
	return GetStringUpToNewLine(multiLine, index);
}

std::string exec(const std::string &cmd) 
{
	std::cout << "exec cmd -> " << cmd << std::endl;
	const int BufferSize = 256;
	std::array<char, BufferSize> buffer;
	std::ostringstream result;
#ifdef WIN32
	std::unique_ptr<FILE, decltype(&_pclose)> pipe(_popen(cmd.c_str(), "r"), _pclose);
#else
	std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd.c_str(), "r"), pclose);
#endif
	if (!pipe)
	{
		throw std::runtime_error("popen() failed!");
	}
	while (!feof(pipe.get()))
	{
		if (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr)
		{
			result << buffer.data();
		}
	}
	std::cout << "exec result : "  << std::endl << result.str() << std::endl;
	return result.str();
}

std::string
GetCommonBase(const std::string &branch1, const std::string &branch2)
{
	// git merge-base branch1 branch2
	std::ostringstream cmd;
	cmd << "git merge-base " << branch1 << " " << branch2;
	std::string commonBase = exec(cmd.str());
	commonBase = GetStringUpToNewLine(commonBase);
	std::cout << "The common base commit is " << commonBase << std::endl;
	return commonBase;
}

std::vector<std::string>
SplitAtNewLines(const std::string &multiline)
{
	size_t index = 0;
	std::vector<std::string> lines;
	while(index != std::string::npos)
	{
		std::string line = GetStringUpToNewLine(multiline, index);
		if(!line.empty())
		{
			lines.push_back(line);
		}
	}

	return lines;
}

std::string
GetFileNameFromPath(const std::string &path)
{
	size_t index = path.rfind('/');
	if(index == std::string::npos)
	{
		return path;
	}
	std::string fileName = path.substr(index + 1);
	return fileName;
}

std::vector<std::string>
GetFilesThatHaveChangedSinceCommit(const std::string &branchName, const std::string &commit)
{
	// git diff --name-only sha1..branchName
	std::ostringstream cmd;
	cmd << "git diff --name-only " << commit << ".." << branchName;
	std::vector<std::string> files;
	std::string diffResult = exec(cmd.str());
	std::cout << "GetFilesThatHaveChangedSinceCommit " << commit << " and " << branchName << std::endl;
	std::cout << diffResult << std::endl;
	files = SplitAtNewLines(diffResult);
	std::vector<std::string>::iterator it = std::unique(files.begin(), files.end());
	unsigned int numberOfUniqueFiles = std::distance(files.begin(), it);
	if(numberOfUniqueFiles != files.size())
	{
		std::cout << "Removing " << files.size() - numberOfUniqueFiles << " duplicates\n";
		files.resize(numberOfUniqueFiles);
	}
	std::sort(files.begin(), files.end());
	return files;
}

std::string
GetCurrentBranch()
{
	std::string cmd = "git rev-parse --abbrev-ref HEAD";
	std::string currentBranch = exec(cmd);
	currentBranch = GetStringUpToNewLine(currentBranch);
	std::cout << "The current branch is " << currentBranch << std::endl;
	return currentBranch;
}

UserInput
DiffFile(const std::string &filePath)
{
	std::cout << "Diff " << filePath << " y, n, or q" << std::endl;
	char input;
	std::cin >> input;
	if((input == 'y') || (input == 'Y'))
	{
		return UserInput::Yes;
	}
	if((input == 'q') || (input == 'Q'))
	{
		return UserInput::Quit;
	}
	return UserInput::No;
}

int main(int argc, char* argv[]) {
	std::cout << "There are " << argc << " arguments" << std::endl;
	std::string tempDir = std::experimental::filesystem::temp_directory_path().string() + TempDirSlash;
	std::cout << "Using " << tempDir << " for all temp files." << std::endl;
	if(argc > 1)
	{
		std::cout << "Running " << argv[0] << std::endl;
		std::string branch1;
		std::string branch2;
		bool workingOnCurrentBranch = false;
		branch1 = argv[1];
		if(argc == 2)
		{
			branch2 = GetCurrentBranch();
			workingOnCurrentBranch = true;
		}
		std::string commonCommit = GetCommonBase(branch1, branch2);
		std::vector<std::string> filesThatChanedInBranch1 = GetFilesThatHaveChangedSinceCommit(branch1, commonCommit);
		std::vector<std::string> filesThatChanedInBranch2 = GetFilesThatHaveChangedSinceCommit(branch2, commonCommit);
		std::vector<std::string> changedInBothBranches = filesThatChanedInBranch1;
		std::vector<std::string>::iterator it = std::set_intersection(	filesThatChanedInBranch1.begin(),
																		filesThatChanedInBranch1.end(),
																		filesThatChanedInBranch2.begin(),
																		filesThatChanedInBranch2.end(),
																		changedInBothBranches.begin());
		changedInBothBranches.resize(it - changedInBothBranches.begin());
		std::cout << "There are " << changedInBothBranches.size() << " files that changed in both branches." << std::endl;
		for(std::string filePath : changedInBothBranches)
		{
			std::string fileName = GetFileNameFromPath(filePath);
			std::cout << filePath << std::endl;
			UserInput userInput = DiffFile(filePath);
			if(userInput == UserInput::Quit)
			{
				break;
			}
			else
			{
				if(userInput == UserInput::No)
				{
					continue;
				}
			}
			std::string tempBaseFileName = tempDir + "CommonBase-" + fileName;
			std::ostringstream getBaseFileCmd;
			getBaseFileCmd << "git show " << commonCommit << ":" << filePath << " > " << tempBaseFileName;
			exec(getBaseFileCmd.str());
			std::string branch1FileName = tempDir  + branch1 + "-" + fileName;
			std::ostringstream getBranch1FileCmd;
			getBranch1FileCmd << "git show " << branch1 << ":" << filePath << " > " << branch1FileName;
			exec(getBranch1FileCmd.str());
			std::string branch2FileName;
			if(workingOnCurrentBranch)
			{
				branch2FileName = filePath;
			}
			else
			{
				branch2FileName = tempDir + branch2 + "-" + fileName;
				std::ostringstream getBranch2FileCmd;
				getBranch2FileCmd << "git show " << branch2 << ":" << filePath << " > " << branch2FileName;
				exec(getBranch2FileCmd.str());
			}
			std::ostringstream diffCmd;
			diffCmd << "meld --diff " << tempBaseFileName << " " << branch1FileName << " " << branch2FileName;
			diffCmd << " --diff " << tempBaseFileName << " " << branch1FileName;
			diffCmd << " --diff " << tempBaseFileName << " " << branch2FileName;
			diffCmd << " --diff " << branch1FileName << " " << branch2FileName;
			std::system(diffCmd.str().c_str());
			std::string removeCommonBaseFile = DeleteFileCommand + " " + tempBaseFileName;
			std::system(removeCommonBaseFile.c_str());
			std::string removeBranch1File = DeleteFileCommand + " " + branch1FileName;
			std::system(removeBranch1File.c_str());
			if(!workingOnCurrentBranch)
			{
				std::string removeBranch2File = DeleteFileCommand + " " + branch2FileName;
				std::system(removeBranch2File.c_str());
			}
		}
	}
	else
	{
		// not enough arguments on the command line
		std::cout << "No branch provided!" << std::endl;
		std::cout << "To compare the current active branch to another:" << std::endl;
		std::cout << "#BranchDiff branchName" << std::endl;
		std::cout << "To compare two branches:" << std::endl;
		std::cout << "#BranchDiff branch1Name branch2Name" << std::endl;
	}
	return 0;
}
